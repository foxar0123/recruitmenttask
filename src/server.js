const express = require ('express');

const app = express();
const fetch = require('node-fetch');
const cors = require('cors');
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const url = 'mongodb://localhost:27017';

const dbname = 'testdb';

const client = new MongoClient(url);


const insertDocuments = function(db, callback) {
  // Get the documents collection
  const collection = db.collection('documents');
  // Insert some documents
  collection.insertMany([
    {a : 1}, {a : 2}, {a : 3}
  ], function(err, result) {
    assert.equal(err, null);
    assert.equal(3, result.result.n);
    assert.equal(3, result.ops.length);
    console.log("Inserted 3 documents into the collection");
    callback(result);
  });
}

async function insertContributor(db,contr,callback){
    const collection = db.collection('contributors');

    console.log("Inserting contributor " + contr.login);
    await collection.update(
        { login: contr.login },
        { $set:
            {
                avatar_url: contr.avatar_url,
                followers: contr.followers,
                public_repos: contr.publicRepos,
                public_gists: contr.publicGists
            }
        },
        {upsert:true}
    );
    callback();
}

const findDocuments = function(db, callback) {
    console.log("Finding documents...");
  // Get the documents collection
  const collection = db.collection('contributors');
  // Find some documents
  collection.find({}).toArray(function(err, docs) {
    assert.equal(err, null);
    console.log("Found the following records");
    console.log(docs)
    collection.countDocuments()
     .then((myCount) =>{
         console.log(myCount);
     });
    callback(docs);
  });
}


client.connect(function(err) {
    assert.equal(null, err);
    console.log("Connected successfully to server");

    const db = client.db(dbname);
    //fetchAll();
    //fetchAllContrDetails();
    fetch('https://api.github.com/rate_limit', myInit).
    then((res2) => res2.json()).
    then((jsonres2) => {
        if(jsonres2.resources.core.remaining <=0){
            console.log('Cant update contributors, API rate limiting!');
            console.log(new Date(jsonres2.resources.core.reset * 1000));
            console.log(jsonres2);
        } else {
            //I couldn't figure out a system to use the fetch and update
            //functions interchangeably, so for now it's only updating existing
            //data and not inserting new.
            updateAll();
            updateContributors();
        }
    });
});
client.close();





const myInit = {
  method: 'GET',
  // mode: 'no-cors',
  cache: 'default',
  headers: {
    'Authorization': 'Basic Rm94YXI6ZTQ2ZjM2M2I4YmVhMDFmYjdjOTMxZjk0NGZkNTM4ODBjYmZiZDNmYQ==',
    'Content-Type': 'application/json',
    'User-Agent': 'Mozilla/5.0 (X11; rarest-achiev; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/76.0',
  },
};



function countCommits(cnt,repo,callback){
    fetch(`https://api.github.com/repos/${repo}/stats/contributors`, myInit).
        then((result) => {
            if(result.ok)
                return result;
            console.log("Error retreiving contributors");
            throw Error(result.statusText);
        }).
        then((res) => res.text()).
	    then((text) => text.length ? JSON.parse(text) : {}).
        then((jsonres) => {
            if(!Array.isArray(jsonres)){
                throw Error('Empty JSON result');
            }
            //console.log(jsonres);
            jsonres.forEach((commit) => {
                //console.log(commit.author.login);
                //console.log(commit.total);
                if( cnt[commit.author.login] == undefined ||
                    cnt[commit.author.login] == NaN)
                    cnt[commit.author.login] = 0;
                cnt[commit.author.login] += commit.total;

            });
            callback();
        },
        (error) =>{
            console.log("Error: " + error)
            throw error;
        }).
        catch(error => {
            console.log("[!]Error: " + error.message);
            callback();
        });
}

async function updateContributors(){
    let counter = {};
    const db = client.db(dbname);
    const collection = db.collection('contributors');
    const collection2 = db.collection('contributor_repos');
    let sortObj ={};
    sortObj['public_repos'] = 1;
    sortObj['public_gists'] = -1;
    sortObj['followers'] = 1;
    let arr=[];
    let a = 0;
    let failures_repos = 0;
    let failures_deets = 0;
    collection.find({}).sort(sortObj).toArray().then((data) => {
        //console.log(data);
        data.forEach((dat)=>{

            console.log();
            console.log("Seeking " + dat.login + "repos");

            console.log(`Fetch https://api.github.com/users/${dat.login}/repos`);

            fetch(`https://api.github.com/users/${dat.login}/repos`, myInit).
                then((res) => {
                    if(res.ok)
                        return res
                    failures_repos++;
                    fetch('https://api.github.com/rate_limit', myInit).
                    then((res2) => res2.json()).
                    then((jsonres2) => {
                        if(jsonres2.resources.core.remaining <=0){
                            console.log('Cant update contributors, API rate limiting!');
                            console.log(new Date(jsonres2.resources.core.reset * 1000));
                            console.log(jsonres2);
                            throw Error("Error updating contributor repos for " + dat.login);
                        }
                        else {
                            console.log(jsonres2);
                        }
                    })
                    return Promise.reject('['+ failures_repos + ']' + 'Error updating contributor repos.');
                }).
                then((result) => result.json()).
                then((jsonres) => {
                    console.log("Will update repos for:");
                    console.log(dat.login);
                    //temprow = {};
                    //console.log(jsonres);
                    if(Array.isArray(jsonres)){
                        jsonres.forEach((repo) => {
                            //console.log(repo);
                            collection2.update(
                                { login: dat.login },
                                { $set:
                                    {
                                        repo: repo
                                    }
                                },
                                {upsert:true}
                            );
                        });
                    }
                    else {
                        console.log("Error: Undefined repo array!");
                        console.log(jsonres);
                    }



                    //insertContributor(client.db(dbname),temprow,() => {});

                }).
                catch(error => console.log(error));

            console.log(`Fetch https://api.github.com/users/${dat.login}`);
            fetch (`https://api.github.com/users/${dat.login}`, myInit).
                then((res) =>{
                    if(res.ok)
                        return res
                    failures_deets++;
                    //console.log(res);
                    fetch('https://api.github.com/rate_limit', myInit).
                    then((res2) => res2.json()).
                    then((jsonres2) => {
                        if(jsonres2.resources.core.remaining <=0){
                            console.log('Cant update contributors, API rate limiting!');
                            console.log(new Date(jsonres2.resources.core.reset * 1000));
                            console.log(jsonres2);
                            throw Error("API Rate limiting");
                        }
                    });
                    return Promise.reject("["+ failures_deets + "]"+"Error updating contributors at fetch for " + dat.login);

                }).
                then((result) => result.json()).
                then((jsonres) => {
                    console.log("Will update:");
                    console.log(dat);
                    //temprow = {};
                    if(jsonres.followers != null && jsonres.followers != undefined){

                        collection.update(
                            { login: dat.login },
                            { $set:
                                {
                                    avatar_url: jsonres.avatar_url,
                                    followers: jsonres.followers,
                                    public_repos: jsonres.public_repos,
                                    public_gists: jsonres.public_gists
                                }
                            },
                            {upsert:true}
                        );
                        //temprow['contributions']=cnt[temprow.login];
                    }
                    else {
                        console.log("Error: Undefined followers!");
                        console.log(jsonres);
                    }



                    //insertContributor(client.db(dbname),temprow,() => {});

                }).
                catch(error => console.log(error));
        });

    });

}

async function fetchRepos(arr, callback){
    let repoCount = 0;
    console.log("Doing" + `https://api.github.com/orgs/angular`);
    await fetch(`https://api.github.com/orgs/angular`, myInit).
        then((res) => res.json()).
        then((jsonres) => {
            if(jsonres.public_repos == undefined){
                console.log('Error, possible Github API rate limiting.');
                console.log(jsonres);
                fetch('https://api.github.com/rate_limit', myInit).
                then((res2) => res2.json()).
                then((jsonres2) => {
                    console.log(jsonres2);
                });
                return 'Error';
            }
            console.log("Retrieved");
            console.log(jsonres);
            repoCount = jsonres.public_repos;
        });
    console.log("Repocount: " + repoCount);
    var failsafe =Math.round(repoCount/100);
    console.log("Failsafe: " + failsafe);
    for(let page=1;page<=failsafe;page++){
        console.log("Doing" + `https://api.github.com/orgs/angular/repos?per_page=100&page=${page}`);
        await fetch(`https://api.github.com/orgs/angular/repos?per_page=100&page=${page}`,myInit).
            then((result) => {
                if(result.ok){
                    return result;
                }
                return 'error';
            }).
            then((res) => res.json()).
            then((jsonres) => {
                //console.log(JSON.stringify(jsonres));
                //console.log(jsonres);
                jsonres.forEach((repo) => {
                    console.log(repo.full_name);
                    arr.push(repo.full_name);
                })
                //let repoName = jsonres.full_name;
                //console.log(jsonres.full_name);
                if(page==failsafe){
                    console.log("Fetch repos callback");
                    callback();
                }
            },
            (error) => {
                console.log(error);
            });
    }
}

async function fetchContDetails(contr,cnt){
    let temprow = {};

    fetch(`https://api.github.com/users/${contr}`,myInit).
        then((userinfo) => userinfo.json()).
        then((userresult) => {
            temprow['followers']=userresult.followers;
            temprow['publicGists']=userresult.public_gists;
            temprow['publicRepos']=userresult.public_repos;
            if( userresult.public_repos == '' ||
                userresult.public_repos == undefined ||
                userresult.public_repos == null ||
                temprow.public_repos == null){
                    console.log(userresult);
                    console.log('null useraaaresult');
                    console.log(temprow);
                }
            temprow['contributions']=cnt[temprow.login];
            insertContributor(client.db(dbname),temprow,() => {});

            },
            (error) => {
                console.log(error);
            },
            ).
            then(()=>{
                findDocuments(client.db('testdb'),()=>{});
            });
}

async function fetchContDetailsFor(repoName,cnt){
    console.log("Fetching for repo" + repoName);
    var temprows = [];
    var contCount = -1;
    await fetch(`https://api.github.com/repos/${repoName}/contributors?per_page=100`,myInit).
        then((result) => {
            if(result.ok){
                return result;
            }
            console.log('Error retrieving contributors from Github API');
        }).
        then((gitres) => gitres.json()).
        then((result2) => {
            temprows = result2;
            contCount =0;
            result2.forEach((temprow) => {
                fetch(`https://api.github.com/users/${temprow.login}`,myInit).
                    then((userinfo) => userinfo.json()).
                    then((userresult) => {
                        console.log("connecting followers");
                        temprow.followers=userresult.followers;
                        temprow.publicGists=userresult.public_gists;
                        temprow.publicRepos=userresult.public_repos;
                        console.log(temprow);
                        console.log(userresult);
                        if( userresult.public_repos == '' ||
                            userresult.public_repos == undefined ||
                            userresult.public_repos == null ||
                            temprow.publicRepos == null){
                                console.log(userresult);
                                console.log('null userresult');
                                console.log(temprow);
                            }
                        temprow.contributions=cnt[temprow.login];
                        insertContributor(client.db(dbname),temprow,() => {
                            contCount++;
                        });

                        },
                        (error) => {
                            console.log(error);
                        },
                    );
            },
            (error) => {
                console.log(error);
            },
            )
        });
}

function fetchAll(){
    console.log("Fetching all");

    var arre = [];

    fetchRepos(arre, () => {
        console.log(arre.length);
        console.log("Done fetching repos");
        let counter ={};
        arre.forEach((a) => {
            countCommits(counter,a,() => {
                console.log("Counting");
                console.log("Fetch cont details for " + a);
                fetchContDetailsFor(a,counter);
            });
        })
    });
};

function updateAll(){
    console.log("Updating all");

    var arre = [];

    fetchRepos(arre, () => {
        const db = client.db(dbname);
        const collection = db.collection('contributors');
        console.log("Done fetching repos");
        let counter ={};
        console.log("Counting");
        let counts =0;
        const CountingPromise = new Promise((resolve, reject) => {
            arre.forEach((a) => {
                console.log("Calling countcommits");
                countCommits(counter,a,() => {
                    console.log("Counting callback");
                    counts++;
                    console.log("Counts: " + counts);
                    if(counts==arre.length){
                        console.log("Resolving");
                        resolve();
                    }
                });

                console.log("Counted commits for " + a);

            });
        }).
        then(() => {
            console.log("Updating database with counter counts");
            Object.keys(counter).forEach((k) => {
                console.log("Update " + k + ": " + counter[k]);
                collection.update(
                    { login: k },
                    { $set:
                        {
                            contributions: counter[k]
                        }
                    },
                    {upsert:true}
                );
            });
        })
    }).
    then(() => {
        console.log("Done updating contributions, updating other contributor statistics.");
        updateContributors();
    });


};

function fetchAllContrDetails(){
    let counter={};
    const db = client.db(dbname);
    const collection = db.collection('contributors');
    collection.find({}).toArray().then((data) => {
        data.forEach((dat)=>{
            countCommits(counter,'angular/angular',() => {
                console.log(counter);
            });
            fetchContDetails(dat.login,counter);
        });
    });
}

app.get('/api/getReposAndContrs', (req,res) => {
    console.log("Requested /api/getReposAndContrs");
    const db = client.db(dbname);
    const collection_repos = db.collection('contributor_repos');
    let repoArray = [];
    var repoContrArray =[];
    let procesedRepos = 0;
    fetchRepos(repoArray, () => {
        repoArray.forEach((repo) => {
            console.log("Doing a find() for " + repo.split('/')[1]);
            collection_repos.find({"repo.name":{$eq: repo.split('/')[1]}}).limit(10).toArray().then((data) => {
                //console.log((data));
                data.forEach((dat) => {
                    if(repoContrArray[dat.repo.name] == undefined || !Array.isArray(repoContrArray[dat.repo.name].logins)){
                        console.log("Initializing repocontrarray logins array");
                        repoContrArray[dat.repo.name] = {
                            logins: []
                        }
                    }
                    repoContrArray[dat.repo.name].logins.push(dat.login);
                });
                procesedRepos++;
            }).
            then(() => {
                if(repoArray.length == procesedRepos){
                    var returnArray= [];
                    Object.keys(repoContrArray).forEach((key) =>{
                        returnArray.push({repo: key, logins: repoContrArray[key]});
                    })

                    return res.send(returnArray);
                }
            });
        })
    });




});

app.get('/api/userRepos', (req,res) => {
    const db = client.db(dbname);
    const collection = db.collection('contributor_repos');
    collection.find({login: req.query.login}).limit(10).toArray().then(rows => {
        return res.send(rows);
    });

});

app.get('/api/localcontributors', async (req,res) =>
{
    await client.connect(function(err) {
        assert.equal(null, err);
        console.log("Connected successfully to server");

        const db = client.db(dbname);
        const collection = db.collection('contributors');
        let sortField = 'login'
        let sortType = 1;

        if(req.query.sortfield!=undefined){
            sortField = req.query.sortfield;
        }
        if(req.query.sorttype=='desc'){
            sortType = -1;
        }else {
            sortType = 1;
        }

        let sortObj ={};
        sortObj[sortField] = sortType;
        collection.find({}).sort(sortObj).limit(50).toArray().then((data) => {
            console.log(data);
            return res.send(data);
        });
    });
});

app.get('/api/contributors', (req,res) =>
{
    fetch(`https://api.github.com/repos/angular/angular/contributors?per_page=3`,myInit).
        then((gitres) => gitres.json()).
        then((result) => {
            console.log(req.query);
            console.log(result);
            return res.send(result);
        },
        (error) => {
            console.log(error);
        },
    );
});

app.get('/api/userdetail', (req,res) =>
{
    fetch(`https://api.github.com/users/${req.query.login}`,myInit).
        then((gitres) => gitres.json()).
        then((result) => {
            console.log(req.query);
            console.log(result);
            return res.send(result);
        },
        (error) => {
            console.log(error);
        },
    );
});

app.listen(process.env.PORT || 8080);
