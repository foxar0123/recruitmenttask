import React from 'react';
import { Box, Avatar, TextField, Button, Typography, Table, TableHead, TableContainer,
     TableCell, TableRow, TableBody, TableSortLabel, Paper, LinearProgress } from '@material-ui/core';
import './App.css';

class UserPage extends React.Component {
      constructor(props) {
        super(props);
        this.state = {
            user: props.user,
            userObj: props.userObj,
            repos: []
        };
    };


      componentDidMount(){
        const myInit = {
          method: 'GET',
          // mode: 'no-cors',
          cache: 'default',
          headers: {
            'Content-Type': 'application/json',
            'User-Agent': 'Mozilla/5.0 (X11; rarest-achiev; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/76.0',
            'Access-Control-Allow-Origin': '*',
          },
        };

        console.log("fetching user repos from backend");
        fetch(`/api/userRepos?login=${this.state.user}`, myInit).
        then((res) => res.json()).
        then((json) => {
            console.log("res:");
            console.log(json);
            this.setState({
                repos:json,
            });

        })
    };


    render() {
        const {user, userObj, repos} = this.state;
        console.log("Rendering userpage");
        console.log("user: " + user);
        console.log("user: " + this.state);
        console.log("Repos:");
        console.log(repos);
        return(
            <span>
              <Box className="userpagebox" bgcolor="info.main">
                <Avatar src={userObj.avatar_url} id="userpageAvatar"/>
                <Typography variant="h2">{userObj.login}</Typography>


                <TableContainer id="userInfoTable">
                <Table size="small">
                <TableBody>
                    <TableRow>
                    <TableCell align="left"><Typography variant="subtitle1">Followers: {userObj.followers}</Typography></TableCell>
                    <TableCell align="left"><Typography variant="subtitle1">Public repositories: {userObj.public_repos}</Typography></TableCell>
                    <TableCell align="left"><Typography variant="subtitle1">Public gists: {userObj.public_gists}</Typography></TableCell>
                    </TableRow>
              </TableBody>
                </Table>
                </TableContainer>

                <Paper square="true">

                <TableContainer component={Paper}>
                <Table size="small">
                <TableHead><TableRow>
                  <TableCell align="left"><TableSortLabel>Repository</TableSortLabel></TableCell>
                  <TableCell align="left"><TableSortLabel>Description</TableSortLabel></TableCell>
                  <TableCell align="left"><TableSortLabel>Language</TableSortLabel></TableCell>
                </TableRow></TableHead>
                <TableBody>{

                    repos.map((repo) => (

                    <TableRow key={repo.repo.name}>
                        <TableCell align="left">{repo.repo.name}</TableCell>
                        <TableCell align="left">{repo.repo.description}</TableCell>
                        <TableCell align="left">{repo.repo.language}</TableCell>
                    </TableRow>

                    ))
              }</TableBody>
                </Table>
                </TableContainer>
                </Paper>
              </Box>

            </span>
        );
    };
};


export default UserPage;
