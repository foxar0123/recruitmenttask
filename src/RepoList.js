import React from 'react';
import { Avatar, TextField, Button, Typography, Table, TableHead, TableContainer,
     TableCell, TableRow, TableBody, TableSortLabel, Paper, LinearProgress, IconButton,
     Collapse, Box } from '@material-ui/core';
import ArrowDropUpOutlinedIcon from '@material-ui/icons/ArrowDropUpOutlined';
import ArrowDropDownOutlinedIcon from '@material-ui/icons/ArrowDropDownOutlined';
import './App.css';


function Row(props) {
  const { row } = props;
  console.log(row);
  console.log(row.logins.logins);
  row.logins.logins.forEach((login) => {
      console.log(login);
  });
  console.log(JSON.stringify(row));
  const [open, setOpen] = React.useState(false);

  return (
    <React.Fragment>
      <TableRow>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <ArrowDropUpOutlinedIcon /> : <ArrowDropDownOutlinedIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row">
          {row.repo}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Typography variant="h6" gutterBottom component="div">
                Contributors
              </Typography>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {row.logins.logins.map((login) => (
                    <TableRow key={login}>
                      <TableCell component="th" scope="row">
                        {login}
                      </TableCell>
                    </TableRow>
                  ))}

                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

class RepoList extends React.Component {
      constructor(props) {
        super(props);
        this.state = {
          rows: props.rows,
        };
        console.log(props);
    };

        componentWillReceiveProps(nextProps){
            console.log("props update");
            console.log(nextProps);
            if(nextProps.rows!==this.props.rows){
                this.setState({
                    rows: nextProps.rows,
                 });

            }
        }

      componentDidMount(){
        const myInit = {
          method: 'GET',
          cache: 'default',
          headers: {
            'Content-Type': 'application/json',
            'User-Agent': 'Mozilla/5.0 (X11; rarest-achiev; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/76.0',
            'Access-Control-Allow-Origin': '*',
          },
        };
    };


    render() {
        const { rows} = this.state;
        console.log("Rows: ");
        console.log(rows);

        if(!Array.isArray(rows)){
            return(<LinearProgress/>);
        }
        else {
            return(
                <span>
                  <div className="content">
                    <TableContainer component={Paper}>
                    <Table size="small">
                    <TableHead><TableRow>
                      <TableCell align="left">>Repo</TableCell>
                    </TableRow></TableHead>
                    <TableBody>{
                        rows.map((row) => (
                            <Row key={row.repo} row={row}/>
                        ))
                  }</TableBody>
                    </Table>
                    </TableContainer>

                  </div>

                </span>
            );
        }
    };
};


export default RepoList;
