import React from 'react';
import { Avatar, TextField, Button, Typography, Table, TableHead, TableContainer,
     TableCell, TableRow, TableBody, TableSortLabel, Paper, LinearProgress } from '@material-ui/core';
import './App.css';

class MainTable extends React.Component {
      constructor(props) {
          console.log("propss:");
          console.log(props);
        super(props);
        this.state = {
          rows: props.rows2,
          orderBy: props.ob,
          orderType: props.ot,
          sortCallback: props.callback,
          clickCallback: props.clickCallback
        };
    };

    componentWillReceiveProps(nextProps){
        console.log("props update");
        console.log(nextProps);
        if(nextProps.rows2!==this.props.rows2){
            //Perform some operation
            this.setState({
                rows: nextProps.rows2,
                orderType: nextProps.ot,
                orderBy: nextProps.ob
             });

        }
    }

      componentDidMount(){
        const myInit = {
          method: 'GET',
          cache: 'default',
          headers: {
            'Content-Type': 'application/json',
            'User-Agent': 'Mozilla/5.0 (X11; rarest-achiev; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/76.0',
            'Access-Control-Allow-Origin': '*',
          },
        };
    };


    render() {
        console.log("Rendering inside maintable");
        const { rows, orderBy, orderType, sortCallback, clickCallback} = this.state;
        return(
            <span>
              <div className="content">
                <TableContainer component={Paper}>
                <Table size="small">
                <TableHead><TableRow>
                  <TableCell align="left"><TableSortLabel onClick={ () => sortCallback('login',orderType)} direction={orderType} active={orderBy == 'contributor'} >Contributor</TableSortLabel></TableCell>
                  <TableCell align="left"><TableSortLabel onClick={ () => sortCallback('contributions',orderType)} direction={orderType} active={orderBy == 'contributions'} >Contributions</TableSortLabel></TableCell>
                  <TableCell align="left"><TableSortLabel onClick={ () => sortCallback('followers',orderType)} direction={orderType} active={orderBy == 'followers'} >Followers</TableSortLabel></TableCell>
                  <TableCell align="left"><TableSortLabel onClick={ () => sortCallback('public_repos',orderType)} direction={orderType} active={orderBy == 'repos'} >Public repositories</TableSortLabel></TableCell>
                  <TableCell align="left"><TableSortLabel onClick={ () => sortCallback('public_gists',orderType)} direction={orderType} active={orderBy == 'gists'} >Public gists</TableSortLabel></TableCell>
                </TableRow></TableHead>
                <TableBody>{
                    rows.map((row) => (

                    <TableRow key={row.login}>
                        <TableCell align="left">
                        <button class="userbutton" onClick={()=> clickCallback(row.login,row)}>
                        <Avatar src={row.avatar_url} />
                        {row.login}
                        </button>
                        </TableCell>
                        <TableCell align="left">{row.contributions}</TableCell>
                        <TableCell align="left">{row.followers}</TableCell>
                        <TableCell align="left">{row.public_repos}</TableCell>
                        <TableCell align="left">{row.public_gists}</TableCell>
                    </TableRow>

                    ))
              }</TableBody>
                </Table>
                </TableContainer>

              </div>

            </span>
        );
    };
};


export default MainTable;
