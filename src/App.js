import React from 'react';
import { Avatar, TextField, Button, Typography, Table, TableHead, TableContainer,
     TableCell, TableRow, TableBody, Paper, LinearProgress } from '@material-ui/core';
import MainTable from './MainTable';
import UserPage from './UserPage';
import RepoList from './RepoList';
import './App.css';
import {BrowserROUTer as Router, Switch, Route, Link} from "react-router-dom";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loadeddata: false,
      loadedfollowers: false,
      followersCount: 0,
      rows: [],
      sortby: 'login',
      sorttype: 'asc',
      page: 0,
      selectedUserLogin: ' ',
      selectedUserObj: {},
      repoListData: {},
    };

    this.orderFunc = this.orderFunc.bind(this);
    this.fetchContributors = this.fetchContributors.bind(this);
    this.clickedOn = this.clickedOn.bind(this);
  }

  fetchContributors(){


    const { sortby, sorttype } = this.state;
    const myInit = {
      method: 'GET',
      // mode: 'no-cors',
      cache: 'default',
      headers: {
        'Content-Type': 'application/json',
        'User-Agent': 'Mozilla/5.0 (X11; rarest-achiev; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/76.0',
        'Access-Control-Allow-Origin': '*',
      },
    };
    console.log("Gonna fetch with sortby " + sortby + " sorttype: " + sorttype);
    fetch(`/api/localcontributors?sorttype=${sorttype}&sortfield=${sortby}`, myInit).
    then((res) => res.json()).
    then((finaldata) => {
      //console.log(finaldata)
      console.log("Setting new rows");
      console.log(this.state.rows);
      this.setState({
          rows: finaldata
      });
      console.log(this.state.rows);
      //console.log(finaldata);
      //console.log(this.state.rows);
    });

    fetch(`/api/getReposAndContrs`, myInit).
    then((res) => res.json()).
    then((jsonres) => {
        console.log("GetReposAndContrs results");
        console.log(jsonres);
        this.setState({
            repoListData: jsonres
        });
    });
  }

  componentDidUpdate(prevProps,prevState){
      console.log("Component updating");
      if(prevState.sortby !== this.state.sortby ||
         prevState.sorttype !== this.state.sorttype ){
          this.fetchContributors();
      }
      console.log("Done updating");

  }

  componentDidMount(){
      console.log("1");
      this.fetchContributors();
      console.log("2");

    }

    orderFunc(by,ot){
        ot=(ot=='asc'?'desc':'asc');
        this.setState({
            sortby: by,
            sorttype: ot
        });
        console.log("Ordertype: " + ot);
        console.log("Orderfunc");
    }
    clickedOn(userlogin, user){
        this.setState({
            selectedUserLogin: userlogin,
            selectedUserObj: user,
            page: 1
        });
    }

  render() {
      console.log("Rendering app");

    const { page, rows, loadedfollowers, followersCount } = this.state;
    console.log(rows);
    if(page==0){
        if(this.state.rows == undefined || this.state.rows.length ==0){
            console.log("Loading");
            return(<LinearProgress/>);
        }
        else{
            console.log("Rendering main table");
            console.log(this.state.rows);
            return (
                <span>
                    <Button onClick={() => {
                        this.setState({
                            page: 2
                        });
                    }}> Repository view </Button>
                    <MainTable clickCallback={this.clickedOn} rows2={rows} ob={this.state.sortby} ot={this.state.sorttype}callback={this.orderFunc}/>
                </span>
            );
        }
    } else if(page==1){
        console.log("Rendering a userpage");
        const {selectedUserLogin, selectedUserObj} = this.state;
        return (
            <span>
            <Button onClick={() => {
                this.setState({
                    page: 0
                });
            }}> Back </Button>
            <UserPage user={selectedUserLogin} userObj={selectedUserObj}/>
            </span>
        );
    } else if(page==2){
        return(
        <span>
        <Button onClick={() => {
            this.setState({
                page: 0
            });
        }}> Back </Button>
        <RepoList rows={this.state.repoListData}/>
        </span>
        );
    }
  }
}

export default App;
